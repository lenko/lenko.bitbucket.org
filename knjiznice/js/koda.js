
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var anaId = "";
var peterId = "";
var markoId = "";



function ustvariPetra(){
    sessionId = getSessionId();
    $.ajaxSetup({
	headers: {"Ehr-Session": sessionId}
		});
    console.log("peter");
    var ime = "Peter";
	var priimek = "Zmaj";
    var datumRojstva = "1880-06-11" + "T00:00:00.000Z";
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        peterId = ehrId;
		        console.log( ehrId + " " + peterId);
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		            },
		            error: function(err) {
		            	alert("napaka pri ustvarjanju Petra Zmaja!");
		            }
		        });
		    }
		});
}
    
function ustvariAno(){
    sessionId = getSessionId();
	$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
    
    console.log("ana");
    var ime = "Ana";
	var priimek = "Banana";
    var datumRojstva = "2008-12-09" + "T00:00:00.000Z";
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        anaId = ehrId;
		        console.log( ehrId + " " + anaId);
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		              //  alert("Ana Banana, ustvarjen!");
		            },
		            error: function(err) {
		            	alert("napaka pri ustvarjanju Ane Banane!");
		            }
		        });
		    }
		});
}    



function ustvariMarka(){
	sessionId = getSessionId();
	$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
    console.log("marko");
    var ime = "Marko";
	var priimek = "Zmeda";
    var datumRojstva = "1990-05-12" + "T00:00:00.000Z";
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        markoId = ehrId;
		        console.log( ehrId + " " + markoId);
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		            //    alert("Marko Zmeda, ustvarjen!");
		            },
		            error: function(err) {
		            	alert("napaka pri ustvarjanju Marka Zmede!");
		            }
		        });
		    }
		});
}










function generirajPodatke() {
    ustvariPetra();
    ustvariAno();
    ustvariMarka();
    
setTimeout(function(){
        
	var datumInUra = "2016-07-14T22:15Z"
	var telesnaVisina = 170;
	var telesnaTeza = 95;
	var telesnaTemperatura = 38;
	var sistolicniKrvniTlak = 130;
	var diastolicniKrvniTlak = 12;
	var nasicenostKrviSKisikom = 98;
	var merilec = "Doktor Miki";

	if (!peterId) {
	console.log("napaka pri dodajanju podatkov o Petru");
	} else {
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    "ehrId": peterId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        console.log("podatki o Petru uspesno dodani");
		        alert("Peter Zmaj ustvarjen! ehrId:"+peterId);
		    },
		    error: function(err) {
		    	console.log("napaka pri dodajanu podatkov o Petru");
		    }
		});
	}
},500);    
    
    
setTimeout(function(){
        
	var datumInUra = "2016-08-01T09:10Z"
	var telesnaVisina = 160;
	var telesnaTeza = 45;
	var telesnaTemperatura = 36;
	var sistolicniKrvniTlak = 110;
	var diastolicniKrvniTlak = 75;
	var nasicenostKrviSKisikom = 95;
	var merilec = "Doktorica Miki";

	if (!anaId) {
	    console.log("napaka pri dodajanju podatkov o Ani");
	} else {
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    "ehrId": anaId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        console.log("podatki o Ani uspesno dodani");
		        alert("Ana Banana ustvarjena! ehrId:"+anaId);
		    },
		    error: function(err) {
		    	console.log("napaka pri dodajanu podatkov o Ani");
		    }
		});
	}
},500);    
    
    
    
setTimeout(function(){
        
	var datumInUra = "2016-07-29T15:30Z"
	var telesnaVisina = 185;
	var telesnaTeza = 80;
	var telesnaTemperatura = 37.5;
	var sistolicniKrvniTlak = 120;
	var diastolicniKrvniTlak = 70;
	var nasicenostKrviSKisikom = 80;
	var merilec = "Doktor Darko";

	if (!markoId) {
	console.log("napaka pri dodajanju podatkov o Marku");
	} else {
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    "ehrId": markoId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        console.log("podatki o Marku uspesno dodani");
		        alert("Marko Zmeda ustvarjen! ehrId:"+markoId);
		    },
		    error: function(err) {
		    	console.log("napaka pri dodajanu podatkov o Petru");
		    }
		});
	}
},500);
    
}








//-------------------------------------------------------------------------------


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Kreiraj nov EHR zapis za pacienta in dodaj osnovne demografske podatke.
 * V primeru uspešne akcije izpiši sporočilo s pridobljenim EHR ID, sicer
 * izpiši napako.
 */
function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
  var datumRojstva = $("#kreirajDatumRojstva").val() + "T00:00:00.000Z";

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		               // console.log(party + " e: " + ehrId );
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}


/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
function vrniId() {
	sessionId = getSessionId();


    var ime = $("#preberiEHRime").val();
    var priimek = $("#preberiEHRpriimek").val();
    
    console.log(ime + " " +priimek);
    var searchData = [{key: "firstNames", value: ime},
    {key:"lastNames", value: priimek}];

		$.ajax({
			url: baseUrl + "/demographics/party/query",
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(searchData),
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
			    for (i in data.parties)
			    {
			        var party = data.parties[i];
			        var id = "";
			        for (j in party.partyAdditionalInfo){
			            if(party.partyAdditionalInfo[j].key === 'ehrId'){
			                id = party.partyAdditionalInfo[j].value;
			                break;
			            }
			        }
			        
			        if (id){
		              $("#vrniId").html("<span class='obvestilo label label-success fade-in'>" +id + "</span>");
			        }
			        
			    }
			    
			},
			error: function(err) {

			}
		});
	
}


/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena kompozicija, ki
 * vključuje množico meritev vitalnih znakov (EHR ID, datum in ura,
 * telesna višina, telesna teža, sistolični in diastolični krvni tlak,
 * nasičenost krvi s kisikom in merilec).
 */
function dodajMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	var telesnaTemperatura = $("#dodajVitalnoTelesnaTemperatura").val();
	var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
	var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
	var nasicenostKrviSKisikom = $("#dodajVitalnoNasicenostKrviSKisikom").val();
	var merilec = $("#dodajVitalnoMerilec").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}


/**
 * Pridobivanje vseh zgodovinskih podatkov meritev izbranih vitalnih znakov
 * (telesna temperatura, filtriranje telesne temperature in telesna teža).
 * Filtriranje telesne temperature je izvedena z AQL poizvedbo, ki se uporablja
 * za napredno iskanje po zdravstvenih podatkih.
 */
function primerjajBolnika() {
	sessionId = getSessionId();

    var ehrId = "";
	ehrId1 = $("#meritveVitalnihZnakovEHRid1").val();
	ehrId2 = $("#meritveVitalnihZnakovEHRid2").val();
    
    var datum1, datum2;
    var leto1, leto2;
    var teza1,teza2;
    var tlaks1, tlaks2;
    var tlakd1, tlakd2;
    var visina1, visina2;
    var starost1, starost2;
    
	if (!ehrId1 || ehrId1.trim().length == 0 || !ehrId2 || ehrId2.trim().length == 0){
	    $("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite Id obeh bolnikov");
	} else {
	    $.ajax({
	      url: baseUrl + "/demographics/ehr/" + ehrId1 + "/party",
    type: 'GET',
    headers: {
        "Ehr-Session": sessionId},  
	        success: function(data){
	        }
	    });
	    
	    $.ajax({
	    url: baseUrl + "/view/" + ehrId1 + "/weight",
        type: 'GET',
        headers: {
            "Ehr-Session": sessionId},
        success: function(data){
            for (var i in data)
            {
                if (!data[i].weight){
                    
                } else{
                teza2 = data[i].weight;
                console.log(teza2); 
                break;
                }
                
            }
        }
	        });
	        
	        
	        
	    $.ajax({
	    url: baseUrl + "/view/" + ehrId2 + "/weight",
        type: 'GET',
        headers: {
            "Ehr-Session": sessionId},
        success: function(data){
            for (var i in data)
            {
                if (!data[i].weight){
                    
                } else{
                teza2 = data[i].weight;
                console.log(teza2);             
                break;
                }
                
            }

        }
	   });
	   
	        
	   	 $.ajax({
	    url: baseUrl + "/view/" + ehrId1 + "/height",
        type: 'GET',
        headers: {
            "Ehr-Session": sessionId},
        success: function(data){
            for (var i in data)
            {
                if (!data[i].height){
                    
                } else{
                visina1 = data[i].height;
                console.log(visina1);                    
                break;
                }
                
            }
        }
	        });
	        
	    $.ajax({
	    url: baseUrl + "/view/" + ehrId2 + "/height",
        type: 'GET',
        headers: {
            "Ehr-Session": sessionId},
        success: function(data){
            for (var i in data)
            {
                if (!data[i].height){
                    
                } else{
                visina2 = data[i].height;
                console.log(visina2);  
                break;
                }
                
            }

        }
	        });
	    
	    
	$.ajax({
    url: baseUrl + "/demographics/ehr/" + ehrId1 + "/party",
    type: 'GET',
    headers: {
        "Ehr-Session": sessionId
    },
    success: function (data) {
        var party = data.party;

        // Date of birth
        var date = new Date(party.dateOfBirth);
        datum1 = date.getMonth() +'. '+ date.getDate() + ', ' + date.getFullYear();
        leto1 = parseInt(date.getFullYear());
        console.log("p "+datum1);
    }
});

	$.ajax({
    url: baseUrl + "/demographics/ehr/" + ehrId2 + "/party",
    type: 'GET',
    headers: {
        "Ehr-Session": sessionId
    },
    success: function (data) {
        var party = data.party;

        // Date of birth
        var date = new Date(party.dateOfBirth);
        datum2 =  date.getMonth() +'. '+ date.getDate() + ', ' + date.getFullYear();
        leto2 = parseInt(date.getFullYear());
        console.log("m "+datum2);
    }
});	    
	    
	    
	        

	        
	  	 $.ajax({
	    url: baseUrl + "/view/" + ehrId1 + "/blood_pressure",
        type: 'GET',
        headers: {
            "Ehr-Session": sessionId},
        success: function(data){
            
            //console.log("blood "+data[0].);
            for (var i in data)
            {
                tlaks1 = data[i].systolic;
                tlakd1 = data[i].diastolic;
                if (!tlaks1 && !tlakd1){
                    
                }else{break;}
                }
            console.log(tlaks1 + " " + tlakd1);
            }
        }
	        );
	        
	  	$.ajax({
	    url: baseUrl + "/view/" + ehrId2 + "/blood_pressure",
        type: 'GET',
        headers: {
            "Ehr-Session": sessionId},
        success: function(data){
            
            //console.log("blood "+data[0].);
            for (var i in data)
            {
                tlaks2 = data[i].systolic;
                tlakd2 = data[i].diastolic;
                if (!tlaks2 && !tlakd2){
                    
                }else{break;}
                }
            console.log(tlaks2 + " " + tlakd2);
            }
        }
	        );
	        
	    
    
    setTimeout(function(){
        
      	var score1= 0;
      	var score2 = 0;
        	
      if (leto1 > leto2 || !leto2)
      {
         score1++;
      }
      if (leto2 > leto1 || !leto1)
      {
          score2++;
      }
      
    if (teza1 < teza2 || !teza2)
      {
         score1++;
      }
      if (teza2 < teza1 || !teza1)
      {
          score2++;
      }
      
      
        if (tlaks1 < tlaks2 || !tlaks2)
      {
         score1++;
      }
      if (tlaks2 < tlaks1 || !tlaks1)
      {
          score2++;
      }
      
      
        if (tlakd1 < tlakd2 || !tlakd2)
      {
         score1++;
      }
      if (tlakd2 < tlakd1 || !tlakd1)
      {
          score2++;
      }
      
      console.log(score1 + " vs " + score2);
      if (score1 < score2)
      {
          $("#rezultatPrimerjave").html("<span class='obvestilo label label-success fade-in'>dlje bo zivel prvi bolnik</span>");
          $("#opozorilo").html("<div>drugemu bolniku priporočamo obisk najbližje bolnice, ki jo lahko najde na spodnjem zemljevidu</div>");
      }
      
      if (score2 < score1)
      {
          $("#rezultatPrimerjave").html("<span class='obvestilo label label-success fade-in'>dlje bo zivel drugi bolnik</span>");
          $("#opozorilo").html("<div>prvemu bolniku priporočamo obisk najbližje bolnice, ki jo lahko najde na spodnjem zemljevidu</div>");
      }
      if (score1 == score2)
      {
       $("#rezultatPrimerjave").html("<span class='obvestilo label label-success fade-in'>ni mogoce predvideti kateri bolnik bo zivel dlje</span>");
       $("#opozorilo").html("<div>obema bolnikoma priporočamo obisk najbližje bolnice, ki jo lahko najdeta na spodnjem zemljevidu</div>");
      }
      
    },500);
    
	    
	    

	}
}


$(document).ready(function() {

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		 var podatki = $(this).val().split(",");
		$("#preberiEHRime").val(podatki[0]);
		$("#preberiEHRpriimek").val(podatki[1]);
	});

  /**
   * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,
   * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,
   * nasičenost krvi s kisikom in merilec) pri vnosu meritve vitalnih znakov
   * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
   */
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoDatumInUra").val(podatki[1]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[2]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[3]);
		$("#dodajVitalnoTelesnaTemperatura").val(podatki[4]);
		$("#dodajVitalnoKrvniTlakSistolicni").val(podatki[5]);
		$("#dodajVitalnoKrvniTlakDiastolicni").val(podatki[6]);
		$("#dodajVitalnoNasicenostKrviSKisikom").val(podatki[7]);
		$("#dodajVitalnoMerilec").val(podatki[8]);
	});



});
